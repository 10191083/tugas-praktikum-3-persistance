package com.syahriah_10191083.persistance;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText input;
    Button save;
    TextView output;
    SharedPreferences sharedPreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        input = findViewById(R.id.input);
        save = findViewById(R.id.save);
        output = findViewById(R.id.output);

        sharedPreferences = getSharedPreferences("Persistance", MODE_PRIVATE);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getData();
                Toast.makeText(MainActivity.this, "Data Telah Disimpan", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void getData(){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("My Data", input.getText().toString());
        editor.apply();
        output.setText("Output Data : " + sharedPreferences.getString("My Data", null));
    }
}